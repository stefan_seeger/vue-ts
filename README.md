# vue-test-ts

![CI](https://github.com/stefanseeger/vue-test-ts/workflows/CI/badge.svg) 
![e2e](https://github.com/stefanseeger/vue-test-ts/workflows/e2e/badge.svg)
![CodeQL](https://github.com/stefanseeger/vue-test-ts/workflows/CodeQL/badge.svg)

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
